package config

import (
	"context"
	"flag"

	"github.com/sethvargo/go-envconfig"
)

type OmdbapiConfig struct {
	ApiURL string `env:"OMDBAPI_URL,default=http://www.omdbapi.com/"`
	ApiKey string `env:"OMDBAPI_KEY"`
}

func NewOmdbapiConfig(ctx context.Context) (*OmdbapiConfig, error) {
	c := &OmdbapiConfig{}

	if err := envconfig.Process(ctx, c); err != nil {
		return nil, err
	}

	flag.StringVar(&c.ApiURL, "omdbapi-url", c.ApiURL, "omdbapi url [OMDBAPI_URL]")
	flag.StringVar(&c.ApiKey, "omdbapi-key", c.ApiKey, "omdbapi key [OMDBAPI_KEY]")

	return c, nil
}
