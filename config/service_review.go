package config

import (
	"context"
	"flag"

	"github.com/sethvargo/go-envconfig"
)

type ReviewConfig struct {
	*SharedConfig `env:",prefix=REVIEW_"`
}

func NewReviewConfig(ctx context.Context) (*ReviewConfig, error) {
	c := &ReviewConfig{}

	if err := envconfig.Process(ctx, c); err != nil {
		return nil, err
	}

	flag.StringVar(&c.Host, "review-host", c.Host, "review host [REVIEW_HOST]")
	flag.IntVar(&c.Port, "review-port", c.Port, "review port [REVIEW_PORT]")

	return c, nil
}
