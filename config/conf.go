package config

import (
	"context"
	"flag"

	"gitlab.com/talgat.s/revue-product/internal/constant"
)

type SharedConfig struct {
	Port int    `env:"PORT,default=80"`
	Host string `env:"HOST"`
}

type Config struct {
	Env     constant.Environment
	Review  *ReviewConfig
	Omdbapi *OmdbapiConfig
	Api     *ApiConfig
}

func NewConfig(ctx context.Context) (*Config, error) {
	conf := &Config{
		Env: getEnv(),
	}

	_ = conf.loadDotEnvFiles()

	// Review config
	if c, err := NewReviewConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.Review = c
	}

	// Omdbapi config
	if c, err := NewOmdbapiConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.Omdbapi = c
	}

	// Api config
	if c, err := NewApiConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.Api = c
	}

	flag.Parse()

	return conf, nil
}
