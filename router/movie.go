package router

import (
	"sync"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/talgat.s/revue-product/router/params"
)

type responses struct {
	review  string
	omdbapi string
	err     error
}

func (r *Router) productGroup(app fiber.Router) {
	rvw := app.Group("/movie")

	rvw.Get("/:id", r.getProduct)
}

func (r *Router) getProduct(c *fiber.Ctx) error {
	ctx := c.UserContext()

	// param
	p := params.IDStr{}
	if err := c.ParamsParser(&p); err != nil {
		return err
	}

	var wg sync.WaitGroup // New wait group
	wg.Add(2)             // Using two goroutines

	jsons := new(responses)

	// review
	go func() {
		if json, err := r.Rvw.SelectProduct(ctx, p.ID); err != nil {
			jsons.err = err
		} else {
			jsons.review = *json
		}
		wg.Done()
	}()

	// omdbapi
	go func() {
		if json, err := r.Omdbapi.SelectMovie(ctx, p.ID); err != nil {
			jsons.err = err
		} else {
			jsons.omdbapi = *json
		}
		wg.Done()
	}()

	wg.Wait()

	if jsons.err != nil {
		return fiber.NewError(fiber.StatusBadRequest, jsons.err.Error())
	}

	return c.JSON(fiber.Map{"review": jsons.review, "movie": jsons.omdbapi})
}
