package router

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"

	"gitlab.com/talgat.s/revue-product/cmd/omdbapi"
	"gitlab.com/talgat.s/revue-product/cmd/review"
)

type Router struct {
	Rvw     *review.Review
	Omdbapi *omdbapi.Omdbapi
}

func (r *Router) Setup(ctx context.Context, app *fiber.App) {
	// Match any request
	app.Use(func(c *fiber.Ctx) error {
		c.SetUserContext(ctx)
		return c.Next()
	})

	api := app.Group("/", logger.New())
	api.Get("/ping", r.ping)

	// product
	r.productGroup(api)
}
