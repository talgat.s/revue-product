package body

type ProductPost struct {
	ID *string `json:"id,omitempty" xml:"id,omitempty" form:"id,omitempty" validate:"required"`
}
