package main

import (
	"context"

	"github.com/sirupsen/logrus"

	"gitlab.com/talgat.s/revue-product/cmd/api"
	"gitlab.com/talgat.s/revue-product/cmd/omdbapi"
	"gitlab.com/talgat.s/revue-product/cmd/review"
	configs "gitlab.com/talgat.s/revue-product/config"
	"gitlab.com/talgat.s/revue-product/internal/constant"
	llog "gitlab.com/talgat.s/revue-product/internal/log"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	// conf
	conf, err := configs.NewConfig(ctx)
	if err != nil {
		logrus.Panic(err)
	}

	// setup logger
	llog.Conf(conf)

	// configure review service
	r := &review.Review{
		Log:  logrus.WithField("service", constant.Review),
		Conf: conf.Review,
	}

	// configure omdbapi service
	a := &omdbapi.Omdbapi{
		Log:  logrus.WithField("service", constant.Omdbapi),
		Conf: conf.Omdbapi,
	}

	// configure gateway service
	srv := &api.Api{
		Log:     logrus.WithField("service", constant.Api),
		Conf:    conf.Api,
		Rvw:     r,
		Omdbapi: a,
	}
	// start gateway service
	srv.StartServer(ctx, cancel)

	<-ctx.Done()
	// Your cleanup tasks go here
	logrus.Info("cleaning up ...")

	logrus.Info("server was successful shutdown.")
}
