package omdbapi

import (
	"context"
	"io"
	"net/http"
	"net/url"
)

func (o *Omdbapi) getMovieURL(id string) (string, error) {
	u, err := url.ParseRequestURI(o.Conf.ApiURL)
	if err != nil {
		return "", err
	}

	q := u.Query()
	q.Add("apikey", o.Conf.ApiKey)
	q.Add("i", id)
	u.RawQuery = q.Encode()

	return u.String(), nil
}

func (o *Omdbapi) SelectMovie(ctx context.Context, id string) (*string, error) {
	if reqURL, err := o.getMovieURL(id); err != nil {
		return nil, err
	} else if req, err := http.NewRequest(http.MethodGet, reqURL, nil); err != nil {
		return nil, err
	} else if res, err := http.DefaultClient.Do(req); err != nil {
		return nil, err
	} else if resBody, err := io.ReadAll(res.Body); err != nil {
		return nil, err
	} else {
		str := string(resBody)
		return &str, nil
	}
}
