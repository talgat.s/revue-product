package omdbapi

import (
	"github.com/sirupsen/logrus"

	configs "gitlab.com/talgat.s/revue-product/config"
)

type Omdbapi struct {
	Log  *logrus.Entry
	Conf *configs.OmdbapiConfig
}
