package log

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/talgat.s/revue-product/config"
	"gitlab.com/talgat.s/revue-product/internal/constant"
)

func Conf(conf *config.Config) {
	if conf.Env != constant.EnvironmentLocal {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}
}
